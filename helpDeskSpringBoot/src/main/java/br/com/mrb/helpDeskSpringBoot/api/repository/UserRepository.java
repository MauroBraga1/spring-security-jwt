package br.com.mrb.helpDeskSpringBoot.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.mrb.helpDeskSpringBoot.api.entity.User;

public interface UserRepository extends MongoRepository<User, String> {

	User findByEmail(String email);

}