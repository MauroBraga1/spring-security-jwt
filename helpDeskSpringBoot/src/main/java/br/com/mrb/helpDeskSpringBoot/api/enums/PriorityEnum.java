package br.com.mrb.helpDeskSpringBoot.api.enums;

public enum PriorityEnum {
	High,
	Normal,
	Low
}